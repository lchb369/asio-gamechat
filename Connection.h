
#ifndef CONNECTION_H
#define	CONNECTION_H

#include <vector>
#include <boost/asio.hpp>
#include <boost/enable_shared_from_this.hpp>
#include "ProtoBuffer.h"
//#include "ConnManager.h"


#define BUFFER_MASK1 (0x59)
#define BUFFER_MASK2 (0x7a)
#define BUFFER_MASK3 (0x7a)
#define BUFFER_MASK4 (0x59)

class ProtoBuffer;
class ConnManager;
class Connection: public boost::enable_shared_from_this<Connection>
{

public:
    Connection(boost::asio::io_service& service , ConnManager& cons );
    ~Connection();
    void startWork();
    void onClose(); 
    void close();
    void onRecv(const boost::system::error_code& err_, size_t bytes_transferred );
	void onSyncRecv();
    void sendMsg( int cmd , const std::string& data );
    void sendMsg( const char* data ,int size );    
    void handleWrite( const boost::system::error_code& ec );

public:
	inline void setSessionID( int sessionId )
	{
		m_sessionId = sessionId;
	}

	inline int getSessionID()
	{
		return m_sessionId;
	}

public:
	boost::asio::ip::tcp::socket  m_socket;
private:
	//std::vector<char> m_readBuffer;
	ProtoBuffer* m_protobuff; 
    ConnManager& m_connects;
	int m_sessionId;

};

typedef boost::shared_ptr<Connection> connPtr;
#endif	/* CONNECTION_H */

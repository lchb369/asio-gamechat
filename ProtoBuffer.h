
#ifndef PROTO_DECODER_H
#define PROTO_DECODER_H

#include <iostream>
#include "boost/shared_ptr.hpp"
#include "MsgHandle.h"
#include "SocketUtil.h"


typedef struct _packMsg
{
	_packMsg(unsigned char _pos, int _size)
	:bufferBucketPos(_pos)
	, bufferSize(_size)
	{};

	_packMsg()
		:bufferBucketPos(0)
		, bufferSize(-1)
	{};

	/** char* position in m_pBufferBucket
	*	when received data, will store the char in m_pBufferBucket, and flag the store position
	*/
	unsigned char	bufferBucketPos;

	/** char* length
	*/
	int				bufferSize;

}PackMsg;



class MsgHandle;
class Connection;
class ProtoBuffer
{
public:
    ProtoBuffer();
    ~ProtoBuffer();

public:
	void callbackRead( boost::shared_ptr<Connection> connect , const char* data , size_t size );

	void printAscii( const char* data , size_t size  );
public:
	char	m_pTmpBuffer[10240];

protected:
	MsgHandle*					m_MsgHandle;
	PackMsg*					m_pRecMsgList;
	unsigned int				m_nRecMsgCount;
	/** received data buffer bucket
	*/
	char*		m_pRecBufferBucket;
	/** m_pRecBufferBucket used size
	*/
	unsigned int	m_nRecBufferBucketUsedSize;
	
	unsigned int	m_tmpBufferPos;

};
#endif

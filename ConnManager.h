
#ifndef CONNECTIONS_H
#define	CONNECTIONS_H

#include <boost/asio.hpp>
#include <set>
#include <algorithm>
#include <boost/shared_array.hpp>
#include "Connection.h"
//class Connection;
class ConnManager
{
public:
    void add(boost::shared_ptr<Connection>& conn);
    
    void remove(boost::shared_ptr<Connection> conn);
    
    void closeAll();
private:
	//setҪ�пո�
    std::set< boost::shared_ptr<Connection> > m_values;
};
#endif

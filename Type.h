
/******************************************************************************
            Copyright (C) Shanda Corporation. All rights reserved.

 This file defines the basic data types used in SGDP, which including 1,2,4,8 bytes
 signed and unsigned data types. In order to avoid the size of the basic type diff-
 renent in various system such as int, the SGDP used all the data types defined here.

******************************************************************************/

#ifndef __TYPE_H__
#define __TYPE_H__
/**
* @file sdtype.h
* @author wangkui
* @brief SGDP数据类型集合
*
**/

#if defined(__linux__)
#define LINUX32
#if defined(__x86_64__)
#define LINUX64
#endif
#endif //__linux__

typedef double DOUBLE;


typedef  unsigned char     BYTE;

#if defined( __linux__ )
typedef  bool              BOOL;
#endif
//
// One byte signed type
//
typedef char         CHAR;

//
// One byte unsigned type
//
typedef unsigned char       UCHAR;

//
// One byte signed type
//
typedef signed char         INT8;

//
// One byte unsigned type
//
typedef unsigned char       UINT8;

//
// Two bytes signed type
//
typedef signed short        INT16;

//
// Two bytes unsigned type
//
typedef unsigned short      UINT16;

//
// Four bytes signed type
//
typedef signed int          INT32;

//
// Four bytes unsigned type
//
typedef unsigned int        UINT32;

//
// Eight bytes signed type
//
typedef signed long long    INT64;

//
// Eight bytes unsigned type
//
typedef unsigned long long  UINT64;

//
//double type
//
typedef double  DOUBLE;

//
//float type
//
typedef float  FLOAT;


//
// Four bytes signed long
//
typedef long   LONG ;

//
// Four bytes unsigned type
//
typedef unsigned long  ULONG;

#endif





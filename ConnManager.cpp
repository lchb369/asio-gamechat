
#include "ConnManager.h"
#include <boost/bind.hpp>

void ConnManager::add(boost::shared_ptr<Connection>& conn )
{
    m_values.insert(conn);
    std::cout << "the number of connections is: " << m_values.size() << std::endl;
}

void ConnManager::remove(boost::shared_ptr<Connection> conn )
{
	if( conn )
	{
        	m_values.erase( conn );
	}
	std::cout << "Connections::Remove :  m_values.size" << m_values.size() << std::endl;
}

void ConnManager::closeAll()
{
	for_each(m_values.begin(), m_values.end(), boost::bind(&Connection::onClose, _1));
        m_values.clear();
}



#include <cstdlib>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <iostream>
#include <vector>
#include <algorithm>
#include "Connection.h"
#include "ConnManager.h"
#include "ProtoBuffer.h"
#include "UserManager.h"
#include "ProtoConf.h"

//#include "LogManager.hpp"

class Server
{
	public:
		Server(
			boost::asio::io_service& io_service , 
			boost::asio::ip::tcp::endpoint const& listen_endpoint 
			)
			:m_io_service( io_service ), 
			m_signals( io_service ), 
			m_acceptor( io_service , listen_endpoint 
		)
		{
			m_signals.add(SIGINT);
			m_signals.add(SIGTERM);
	#if defined(SIGQUIT)
			m_signals.add(SIGQUIT);
	#endif
			m_signals.async_wait(boost::bind(&Server::stop, this));
			boost::shared_ptr<Connection> conn( new Connection( m_io_service, m_connects ));
        

			std::cout<<"conn reference:"<<conn.use_count()<<std::endl;
			m_acceptor.async_accept( conn->m_socket,
					boost::bind( &Server::afterAccept, this, conn , _1));
		}

		void run()
		{
			m_io_service.run();
		}

		void afterAccept( boost::shared_ptr<Connection>& connect, boost::system::error_code const& error )
		{
			std::cout<<"AfterAccept conn reference:"<<connect.use_count()<<std::endl;
			// Check whether the server was stopped by a signal before this completion
			// handler had a chance to run.
			if (!m_acceptor.is_open())
			{
				return;
			}
        
			if ( !error )
			{
				m_connects.add( connect );
				connect->startWork();
				//创建新的接收句柄，以容纳新到来的连接
				boost::shared_ptr<Connection> newConn( new Connection( m_io_service, m_connects ));
				m_acceptor.async_accept( newConn->m_socket,
				boost::bind(&Server::afterAccept, this, newConn , _1 ) );
				
			}
		}

	private:

		void stop()
		{
			std::cout << "stopping" << std::endl;
			m_acceptor.close();
			m_connects.closeAll();
			m_io_service.stop();
			// log_mgr.close();
		}

	private:
		boost::asio::io_service& m_io_service;
		boost::asio::signal_set m_signals;
		boost::asio::ip::tcp::acceptor m_acceptor;
		ConnManager m_connects;
};

int main(int argc, char** argv)
{
	
	//LogManager::Instance()->open();
	//log_ptr mainLog = LogManager::Instance()->createLog( "/tmp/" , RUN_LOG );
	//mainLog->enablePrintScreen( true );
	//mainLog->enablePrintFile( true );
	//LOG_DEBUG( RUN_LOG, "service" , "start" );
	//LOG_ERROR( RUN_LOG, "error" , "xxxxxxxxxxxxxxxxx" );
	

	if( ProtoConf::loadXml() == false )
	{
		std::cout<<"load Proto Config Error"<<std::endl;
	}

	if( UserManager::createInstance() == false )
	{
		//LOG_ERROR( RUN_LOG , "notify_service" , "Create UserManager Faild!" );
		return -1;
	}

	boost::asio::io_service io_service;
	boost::asio::ip::tcp::endpoint listen_endpoint( boost::asio::ip::tcp::v4() , 2345 );
    Server server( io_service , listen_endpoint );
    server.run();
    return 0;
}



#ifndef PROTO_CONFIG_H_
#define PROTO_CONFIG_H_

#include "Type.h"
#include <iostream>
#include <vector>
#include <map>

using namespace std;
typedef struct _XmlField
{
  string name;
  string type;
} XmlField;

typedef vector<XmlField> XmlFields;
typedef map<UINT32,XmlFields> mapConfig;

class ProtoConf
{
    public:
		static bool loadXml();
		static mapConfig getProtoConf()
		{
			return m_conf;
		}
		static XmlFields getFields( UINT32 cmd );
   private:
		static mapConfig  m_conf;
};

#endif



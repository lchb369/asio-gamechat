
#include "UserManager.h"
#include <boost/bind.hpp>


IMPLEMENT_SINGLETON(UserManager)
void UserManager::add( int uid , User* user )
{
	m_mapConn[uid] = user;
    std::cout << "the number of UserManager is: " << m_mapConn.size() << std::endl;
}

void UserManager::remove( int uid )
{
       std::map<int,User*>::iterator it;      
       it = m_mapConn.find( uid );
       if( it != m_mapConn.end() )
       {
            m_mapConn[uid]->conn->close();
	    delete m_mapConn[uid];
	    m_mapConn.erase( uid );
      }
      std::cout << "UserManager Remove :  m_values.size" << m_mapConn.size() << std::endl;
}

void UserManager::clear()
{
	m_mapConn.erase( m_mapConn.begin(), m_mapConn.end() );
	std::cout << "UserManager :  m_values.size" << m_mapConn.size() << std::endl;
}

/**
	get User* obj by uid
**/
User* UserManager::get( int uid )
{
	std::map<int, User*>::iterator it;
	it = m_mapConn.find(uid);
	if (it != m_mapConn.end())
	{
		return m_mapConn[uid];
	}
	return NULL;
}


/**
 * check online user in a uids vector, and remove outline uids ,return online user num;
**/
void UserManager::getOnline( vector<string> vecUids , vector<string> &onlines )
{
	if ( vecUids.size() > 0)
	{
		vector<string>::iterator vit;
		std::map<int, User*>::iterator mit;
		for ( vit = vecUids.begin(); vit != vecUids.end(); ++vit )
		{
			mit = m_mapConn.find( atoi((*vit).c_str()) );
			if ( mit != m_mapConn.end() )
			{
				onlines.push_back( *vit );
			}
		}
	}
}



void UserManager::sendTo( int uid, const char* data, int len)
{
	std::map<int, User*>::iterator it;
	for (it = m_mapConn.begin(); it != m_mapConn.end(); ++it)
	{
		if (uid == it->first)
		{
			it->second->conn->sendMsg(data, len);
		}
	}
}

//广播给所有服的所有人
void UserManager::broadCast( const char* data , int len )
{
   std::map<int,User*>::iterator it; 
   for( it = m_mapConn.begin(); it != m_mapConn.end(); ++it )
   {
	 it->second->conn->sendMsg( data , len );	
   }	
}


void UserManager::broadCast(  char ct , int cid , const char* data , int len )
{
   std::cout << "broadcast UserManagers is: " << m_mapConn.size() << std::endl;
   std::map<int,User*>::iterator it; 
   for( it = m_mapConn.begin(); it != m_mapConn.end(); ++it )
   {
	 std::cout<<"UserManager uid=="<<it->first<<std::endl;
	   if( ct == CHANNAL_SERVER && cid == it->second->sid )
	   {
	       	   std::cout<<"server buffer size:"<<"UserManager uid=="<<it->first<<std::endl;
		   it->second->conn->sendMsg( data, len );
	   }
	   else if( ct == CHANNAL_GANG && cid == it->second->gangId )
	   {
		   it->second->conn->sendMsg( data , len );
	   }
	   else
	   {
		  // LOG_ERROR( RUN_LOG , "server" , "BroadCast Channal Mssage Error!!");
	   }
   }	
}




#ifndef USER_MANAGER_H
#define	USER_MANAGER_H

#include <boost/asio.hpp>
#include <map>
#include <algorithm>
#include <boost/shared_array.hpp>
#include "Connection.h"
#include "Singleton.h"

struct User
{
    boost::shared_ptr<Connection> conn;
    int uid;
    int sid;
    int gangId;
    std::string uname;
};


class UserManager
{
public:
	DECLARE_SINGLETON( UserManager )

public:
	
    void add( int uid, User* user );    

    User* get( int uid );

    void getOnline( vector<string> vecUids , vector<string> &onlines );

    void remove( int uid );
    
    void clear();
	
	void sendTo( int uid, const char* data, int len );

    void broadCast( char ct , int cid , const char* data , int len );
   
    void broadCast( const char* data , int len  );

private:
    std::map<int, User*> m_mapConn;
};
#endif

/*
* =====================================================================================
*
*       Filename:  ProtoConf.cpp
*
*    Description:  i
*
*        Version:  1.0
*        Created:  12/11/2013 04:00:06 PM
*       Revision:  none
*       Compiler:  gcc
*
*         Author:  Changbing.Liu (), lchb@qq.com
*   Organization:  
*
* =====================================================================================
*/

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp> 
#include <boost/foreach.hpp>
#include <boost/typeof/typeof.hpp>  

#include "ProtoConf.h"

mapConfig ProtoConf::m_conf;

bool ProtoConf::loadXml()
{
	using boost::property_tree::ptree;
	ptree pt;
	ptree dataSet;
	try
	{ 
		read_xml( "/home/test6/protos.xml" , pt );
		dataSet = pt.get_child( "NewDataSet" );

	}  
	catch (std::exception& e)
	{
		std::cout<<"xxx"<<std::endl;
		return false;
	}

	for( ptree::iterator itr = dataSet.begin(); itr != dataSet.end();  ++itr )
	{
		ptree methods = itr->second;
		UINT32 mid = methods.get<UINT32>( "methods_id" );
		ptree pofields = methods.get_child( "ofields" );
		vector<XmlField> vecFields;
		for( BOOST_AUTO( pos, pofields.begin() ); pos != pofields.end(); ++pos )  //boost中的auto  
		{
			ptree fields = pos->second;
			XmlField field;
			field.name = fields.get<string>( "fieldname" );
			field.type = fields.get<string>( "fieldtype" );
			vecFields.push_back( field );	
		}
		m_conf.insert( pair<UINT32,XmlFields>( mid , vecFields ));
	}
	return m_conf.empty() ? false : true;
}

XmlFields ProtoConf::getFields( UINT32 cmd )
{
	map<UINT32,XmlFields>::iterator it = m_conf.find(cmd);
	if (it != m_conf.end())
		return it->second;

	return XmlFields();
}

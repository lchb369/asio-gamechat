
#include "MsgHandle.h"
#include "ConnManager.h"
#include "UserManager.h"
#include <vector>
#include "Type.h"
#include <stdarg.h>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string.hpp>

#include "ProtoConf.h"
using namespace std;

//login
#define CMD_LOGIN_REQ 1001
#define CMD_LOGIN_RES 2001

//message
#define CMD_MSG_REQ 1002
#define CMD_MSG_RES 2002

//private message
#define CMD_PRIVATE_MSG_REQ  1003
#define CMD_PRIVATE_MSG_RES  2003

//get friend online list
#define CMD_CHECK_ONLINE_USERS_REQ  1004
#define CMD_CHECK_ONLINE_USERS_RES  2004


MsgHandle::MsgHandle()
{
	memset( m_writerBuffer , 0 , sizeof( m_writerBuffer ));
	std::cout<<"MsgHandle construct ..."<<std::endl;
}

MsgHandle::~MsgHandle()
{
	//std::cout<<"MsgHandle destruct ..."<<std::endl;
}

//包头:2位包头掩码+4位包长
//包体:1位加密+包体数据
//包尾:2
void MsgHandle::dispacher(   const char* buffer, int len , boost::shared_ptr<Connection> connect )
{
	NZBufferReader reader(  buffer , 4096 , 0 );

	unsigned int cmd = reader.readUint();
	cout<<"cmd:"<<cmd<<endl;
	
	//登录请求
	if( cmd == CMD_LOGIN_REQ )
	{
		User* user = new User;
		user->conn = connect;
		user->uid = reader.readUint();
		user->sid = reader.readUint();
		user->gangId = reader.readUint();
		user->uname = reader.readString();

		connect->setSessionID( user->uid );
		UserManager::instance()->add( user->uid , user );

		cout<<"uid"<<user->uid<<endl;
		cout<<"sid"<<user->sid<<endl;
		cout<<"gangId"<<user->gangId<<endl;
		cout<<"uname"<<user->uname<<endl;
		
		NZBufferWriter writer = packBuffer( CMD_LOGIN_RES , 1 );
		connect->sendMsg( writer.getBuffer() , writer.getBufferSize() );

	}
	//频道消息
	else if( cmd == CMD_MSG_REQ )
	{
		char channal = reader.readChar();
		UINT32 cid = reader.readUint();
		string msg = reader.readString();
		UINT32 uid = connect->getSessionID();
		User* user = UserManager::instance()->get( uid );

		cout<<"sid"<<user->sid<<endl;
		cout<<"uid"<<user->uid<<endl;
		cout<<"uname"<<user->uname<<endl;
		cout<<"channal"<<channal<<endl;
		cout<<"cid"<<cid<<endl;
		cout<<"===========msg========="<<msg<<endl;

		if (user->uid > 0 && !user->uname.empty())
		{
			NZBufferWriter writer = packBuffer(CMD_MSG_RES, user->sid, user->uid, user->uname.c_str(), channal, cid, msg.c_str());
			UserManager::instance()->broadCast(  channal, cid, writer.getBuffer(), writer.getBufferSize());
		}
	}
	//私聊消息
	else if ( cmd == CMD_PRIVATE_MSG_REQ)
	{
		UINT32 toUid = reader.readUint();
		string msg = reader.readString();
		UINT32 uid = connect->getSessionID();
		User* user = UserManager::instance()->get(uid);

		cout << "sid" << user->sid << endl;
		cout << "uid" << user->uid << endl;
		cout << "uname" << user->uname << endl;
		cout << "===========private msg=========" << msg << endl;

		if (user->uid > 0 && !user->uname.empty())
		{
			NZBufferWriter writer = packBuffer( CMD_PRIVATE_MSG_RES , user->uid, user->uname.c_str(), msg.c_str());
			UserManager::instance()->sendTo( toUid, writer.getBuffer(), writer.getBufferSize());
		}
	}
	//检查在线列表
	else if ( cmd == CMD_CHECK_ONLINE_USERS_REQ )
	{
		string uidsStr = reader.readString();
		string onlineUserStr;
		vector<string> vecUids;
		vector<string> vecOnlines;
	
		boost::split( vecUids , uidsStr , boost::is_any_of(",") , boost::token_compress_on );
	
		//用一个vector来存uid
		UserManager::instance()->getOnline( vecUids , vecOnlines  );
		onlineUserStr = boost::join( vecOnlines , ",");
		
		if( onlineUserStr.empty() )
		{
			onlineUserStr = "NULL";
		}
	
		cout<<"result:"<<onlineUserStr<<endl;
		NZBufferWriter writer = packBuffer( CMD_CHECK_ONLINE_USERS_RES, onlineUserStr.c_str());
		connect->sendMsg(writer.getBuffer(), writer.getBufferSize());
		
	}

}



NZBufferWriter MsgHandle::packBuffer( UINT32 cmd , ... )
{
	va_list va;
	va_start( va, cmd );
	XmlFields confFields = ProtoConf::getFields( cmd );
	NZBufferWriter writer( m_writerBuffer , sizeof(m_writerBuffer)  , 0 );
	NZSocketUtil::writePackageHeader( &writer );
	
	writer.writeUint( 0 );
	writer.writeChar( 0 );
	writer.writeUint( cmd );
	cout<<" confFields.size()"<< confFields.size() <<endl;

	for( UINT32 i = 0; i  < confFields.size(); i++  )
	{
		if( !confFields[i].type.compare( "uint" ) )
		{
			writer.writeUint( va_arg( va, UINT32 ) );
		}
		else if( !confFields[i].type.compare( "int" ) )
		{
			writer.writeInt( va_arg( va, INT32 ) );
		}
		else if( !confFields[i].type.compare( "float" ) )
		{
			//not support var type of char,float on linux platform
			writer.writeFloat( (float)va_arg( va, double ) );
		}
		else if(  !confFields[i].type.compare( "char" ) )
		{
			//not support var type of char,float on linux platform
			char tmp[2];
			sprintf( tmp , "%c" , va_arg( va, int ) );
			writer.writeChar( tmp[0]  );
		}
		else if( !confFields[i].type.compare( "string" ) )
		{
			cout<<"string ....char..."<<endl;
			writer.writeString( va_arg( va, CHAR* ) );
		}
	}
	va_end(va);
	NZSocketUtil::writePackageFooter( &writer );
	UINT32 nPackBodySize = writer.getBufferSize() - 9;
	memcpy(m_writerBuffer + 2, &nPackBodySize, sizeof(nPackBodySize));
	return writer;

}

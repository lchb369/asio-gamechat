
#include "ProtoBuffer.h"
#include "Connection.h"
#include "MsgHandle.h"

#define PACK_HEAD_LEN (8)
#define PACK_MSG_LIST_NUM (100)
#define REC_BUFFER_BUCKET_MAX_SIZE (10240) //10k
#define SEND_BUFFER_BUCKET_MAX_SIZE (102400) //100k
#define SOCKET_READ_BUFFER_SIZE (64) //64byte
#define SOCKET_TMP_BUFFER_SIZE (10240) //64byte
#define PACK_HEADER_LEN (7)
#define PACK_FOOTER_LEN (2)


ProtoBuffer::ProtoBuffer()
{
	unsigned int nMsgListSize = PACK_MSG_LIST_NUM * sizeof(PackMsg);
	m_pRecMsgList = (PackMsg*)malloc(nMsgListSize);
	memset(m_pRecMsgList, 0, nMsgListSize);
	m_nRecMsgCount = 0;
	m_pRecBufferBucket = (char*)malloc( REC_BUFFER_BUCKET_MAX_SIZE );
	m_nRecBufferBucketUsedSize = 0;
	m_tmpBufferPos = 0;
	m_MsgHandle = new MsgHandle;	
	//m_pTmpBuffer = (char*)malloc( SOCKET_TMP_BUFFER_SIZE );
	memset( m_pTmpBuffer, 0, SOCKET_TMP_BUFFER_SIZE);
	std::cout<<"ProtoBuffer construct ..."<<std::endl;

}

ProtoBuffer::~ProtoBuffer()
{
	delete m_MsgHandle;


	if (m_pRecMsgList)
	{
		free(m_pRecMsgList);
		m_pRecMsgList = NULL;
	}

	if (m_pRecBufferBucket)
	{
		free(m_pRecBufferBucket);
		m_pRecBufferBucket = NULL;
	}
	
	m_tmpBufferPos = 0;
	/*
	if (m_pTmpBuffer)
	{
		free(m_pTmpBuffer);
		m_pTmpBuffer = NULL;
	}
	*/
	//std::cout<<"ProtoBuffer destruct ..."<<std::endl;
}

void ProtoBuffer::callbackRead( boost::shared_ptr<Connection> connect , const char* data , size_t size )
{

	printAscii( m_pTmpBuffer , size );
    	std::cout<< "================m_tmpBufferPos"<<m_tmpBufferPos<<"m_pTmpBuffer"<<"size"<<size<<m_pTmpBuffer<<std::endl;
	
	if ( size > 0 )
        {
		m_tmpBufferPos += size;
		unsigned int i = 0;
		while (i < m_tmpBufferPos )
		{
			//is header
			if (!NZSocketUtil::isPackageHeaderFlag2(m_pTmpBuffer + i))
			{
				i++;
				continue;
			}
			if( i == 0 )
			{
				i++;
				continue;
			}
			if (!NZSocketUtil::isPackageHeaderFlag1(m_pTmpBuffer + i - 1))
			{
				i++;
				continue;
			}
			//at header
			//if cannot read the package length
			if (i + 1 + sizeof(unsigned int)> m_tmpBufferPos)
			{
				break;
			}
			
			//read package size
			cout<<"i==="<<i<<endl;
		//	i = ( i==0 ) ? 1 : i;
			unsigned int nPackSize = 0;
			memcpy(&nPackSize, m_pTmpBuffer + i + 1, sizeof(unsigned int));

			  cout<<"===1.2==="<<i<<endl;
			unsigned int nPackStartPos = i - 1;
			unsigned int nPackEndPos = nPackStartPos + PACK_HEADER_LEN + nPackSize + PACK_FOOTER_LEN;

			//if cannot read the whole package or not normal package end then continue
			if (nPackEndPos > m_tmpBufferPos ||
				!NZSocketUtil::isPackageHeaderFlag3(m_pTmpBuffer + nPackEndPos - 2) ||
				!NZSocketUtil::isPackageHeaderFlag4(m_pTmpBuffer + nPackEndPos - 1)
				)
			{
				cout<<"===1.3==="<<nPackEndPos<<">"<<m_tmpBufferPos<<"===="<<endl;
				break;
			}

				
			  cout<<"===2==="<<i<<endl;		
	
			//20  =  2 + 4 + 12 + 2
			//push msg

			char* pBuffer = m_pRecBufferBucket + m_nRecBufferBucketUsedSize;
			memcpy(pBuffer, 
				m_pTmpBuffer + nPackStartPos + PACK_HEADER_LEN,
				nPackSize
			);
			


			 cout<<"===3==="<<i<<endl;
			//PrintAscii( pBuffer , 12 );
			m_MsgHandle->dispacher(  pBuffer , nPackSize , connect );       
			//break;
			//this must first
			/*
			PackMsg msg(m_nRecBufferBucketUsedSize, nPackSize);
			//then update m_nRecBufferBucketUsedSize
			m_nRecBufferBucketUsedSize += size;
			memcpy(m_pRecMsgList + m_nRecMsgCount, &msg, sizeof(PackMsg));
			m_nRecMsgCount++;
			*/
			//memset( m_pTmpBuffer, 0 , sizeof( m_pTmpBuffer ) );
			//m_tmpBufferPos = 0;
			//i = 0;
			
			memcpy(m_pTmpBuffer + nPackStartPos, 
				m_pTmpBuffer + nPackStartPos + PACK_HEADER_LEN + nPackSize + PACK_FOOTER_LEN,
				m_tmpBufferPos - nPackEndPos
				);
			m_tmpBufferPos -= PACK_HEADER_LEN + nPackSize + PACK_FOOTER_LEN;

			i = nPackStartPos;
			
		}
	}
	//m_MsgHandle->Dispacher( m_head , m_body , connect );       
}


void ProtoBuffer::printAscii( const char* data , size_t size )
{
	 std::cout<<"ASCII"<<std::endl;
	 size_t loop;
	 for(loop=0; loop < size; loop++ )
	 {
		int asc = data[loop];
		printf(" %d", asc );
	 }
}


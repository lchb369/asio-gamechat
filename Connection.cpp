
#include "Connection.h"
#include <boost/bind.hpp>
#include "ConnManager.h"
#include "SocketUtil.h"
#include "UserManager.h"

Connection::Connection( boost::asio::io_service& service , ConnManager& cons )
	:m_socket( service ),
	m_connects( cons )
{
	 std::cout << "Connection...." << std::endl;
	m_protobuff = new ProtoBuffer;
}

Connection::~Connection()
{
    delete m_protobuff;
    std::cout << "~Connection" << std::endl;
}

void Connection::startWork()
{
	m_socket.async_read_some( boost::asio::buffer( m_protobuff->m_pTmpBuffer ),
		boost::bind(&Connection::onRecv, shared_from_this(), 
		boost::asio::placeholders::error,                                                  
		boost::asio::placeholders::bytes_transferred )
	);
}

void Connection::onClose()
{
	std::cout << "onClose the socket"<< m_sessionId <<std::endl;
	UserManager::instance()->remove( m_sessionId );
	//m_socket.shutdown( boost::asio::ip::tcp::socket::shutdown_both );
	//m_socket.close();
	//m_connects.remove( shared_from_this() );
}

void Connection::close()
{
	try{
        	m_socket.shutdown( boost::asio::ip::tcp::socket::shutdown_both );
		std::cout << "closing the socket"<< m_sessionId <<std::endl;
	}catch (std::exception& e) {  
    		std::cout<< e.what() <<std::endl;
  	}
	m_socket.close();
        m_connects.remove( shared_from_this() );
}


//接收消息
void Connection::onRecv( const boost::system::error_code& err, size_t bytes_transferred ) 
{
    	if (err)
	{
        	std::cout<<" Connection::OnRecv errmsg:"<< err.message()  << std::endl;
		onClose();
        	return;
   	}
	else
	{
		std::cout << "Connection::OnRecv data bytes_transferred:" <<bytes_transferred<< std::endl;
		m_protobuff->callbackRead( shared_from_this() , m_protobuff->m_pTmpBuffer , bytes_transferred );
		m_socket.async_read_some( boost::asio::buffer(m_protobuff->m_pTmpBuffer),
			boost::bind( &Connection::onRecv, shared_from_this(), 
			boost::asio::placeholders::error,                                                  
			boost::asio::placeholders::bytes_transferred )
		);
		
	}
}

//同步接收消息
void Connection::onSyncRecv() 
{
	boost::system::error_code err;
	size_t len;
	while( true )
	{
		std::cout<<" Start Recv " << std::endl;
		len = m_socket.read_some(boost::asio::buffer(m_protobuff->m_pTmpBuffer), err );
		if (err)
		{
			std::cout<<" Connection::OnRecv errmsg:"<< err.message()  << std::endl;
			onClose();
			return; 
		}
		else
		{
			std::cout<<" Connection::OnSyncRecv Len:"<<len<< std::endl;
			m_protobuff->callbackRead( shared_from_this() ,m_protobuff->m_pTmpBuffer , len );
		}
	}
	
}

void Connection::sendMsg(const char* data , int size )
{
	cout<<"send ascii:"<<size<<endl;
	int loop;
    	for(loop=0; loop <= size; loop++ )
    	{
       	 	int asc = data[loop];
       		 printf(" %d", asc );
    	}

	cout<<"send len:"<<size<<endl;
	boost::asio::async_write(m_socket,
	boost::asio::buffer( data , size ),
	boost::bind( &Connection::handleWrite, shared_from_this(),
	boost::asio::placeholders::error));
}

void Connection::handleWrite( const boost::system::error_code& ec )
{
	if( ec )
	{
		std::cout<<"Send Msg Error:"<< ec.message() << std::endl;
	}	
}

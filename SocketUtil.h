
/* ***************************************************************************
NZSocketUtil
============================================================================
Copyright (c) 2013 NuoYou
Create By Tungway 2013-12-09
Update By Tungway 2013-12-11
****************************************************************************/

#ifndef __XC_NZSOCKETUTIL_H__
#define __XC_NZSOCKETUTIL_H__
#include <string>
#include <time.h>

using namespace std;


#define UINT_SIZE (sizeof(unsigned int))
#define INT_SIZE (sizeof(int))
#define CHAR_SIZE (sizeof(char))
#define FLOAT_SIZE (sizeof(float))
#define BUFFER_MASK1 (0x59)
#define BUFFER_MASK2 (0x7a)
#define BUFFER_MASK3 (0x7a)
#define BUFFER_MASK4 (0x59)
#define BUFFER_FLAG_MAX (1000)

class NZBufferWriter
{
public:
	NZBufferWriter(char* pOutBuffer, const unsigned int& maxSize, const unsigned int& pos)
		:m_pBuffer(pOutBuffer)
		, m_maxSize(maxSize)
		, m_pos(pos)
	{
	};

	NZBufferWriter()
		:m_pBuffer(NULL)
		, m_maxSize(0)
		, m_pos(0)
	{};

	const char* getBuffer()
	{
		return m_pBuffer;
	}

	unsigned int getBufferSize()
	{
		return m_pos;
	};

	/* * write a unsigned int number
	*/
	void writeUint(const unsigned int& value)
	{
		memcpy(m_pBuffer + m_pos, &value, UINT_SIZE);
		m_pos += UINT_SIZE;
	};

	/* * write a int number
	*/
	void writeInt(const int& value)
	{
		memcpy(m_pBuffer + m_pos, &value, INT_SIZE);
		m_pos += INT_SIZE;
	};

	/* * write a float number
	*/
	void writeFloat(const float& value)
	{
		memcpy(m_pBuffer + m_pos, &value, FLOAT_SIZE);
		m_pos += FLOAT_SIZE;
	};

	/* * write a float number
	*/
	void writeChar(const char& value)
	{
		memcpy(m_pBuffer + m_pos, &value, CHAR_SIZE);
		m_pos += CHAR_SIZE;
	};

	/* * write a string with length
	*/
	void writeString(const char* pString)
	{
		unsigned int nLen = strlen(pString) + 1;
		writeUint(nLen);
		memcpy(m_pBuffer + m_pos, pString, nLen);
		m_pos += nLen;
	};

private:
	char*			m_pBuffer;
	unsigned int	m_maxSize;
	unsigned int    m_pos;
};

class NZBufferReader
{
public:
	NZBufferReader(const char* pOutBuffer, const unsigned int& maxSize, const unsigned int& pos)
		:m_pBuffer(pOutBuffer)
		, m_maxSize(maxSize)
		, m_pos(pos)
	{
	};

	NZBufferReader()
		:m_pBuffer(NULL)
		, m_maxSize(0)
		, m_pos(0)
	{};

	unsigned int getBufferAvailableSize()
	{
		return m_maxSize - m_pos;
	};

	unsigned int readUint()
	{
		unsigned int nValue = 0;
		memcpy(&nValue, m_pBuffer + m_pos, UINT_SIZE);
		m_pos += UINT_SIZE;
		return nValue;
	};

	int readInt()
	{
		int nValue = 0;
		memcpy(&nValue, m_pBuffer + m_pos, INT_SIZE);
		m_pos += INT_SIZE;
		return nValue;
	};

	float readFloat()
	{
		float nValue = 0;
		memcpy(&nValue, m_pBuffer + m_pos, FLOAT_SIZE);
		m_pos += FLOAT_SIZE;
		return nValue;
	};

	char readChar()
	{
		char nValue = 0;
		memcpy(&nValue, m_pBuffer + m_pos, CHAR_SIZE);
		m_pos += CHAR_SIZE;
		return nValue;
	};

	string readString()
	{
		const unsigned int nLen = readUint();
		char pStr[2048];
		memset(pStr, 0, sizeof(pStr));
		memcpy(pStr, m_pBuffer + m_pos, nLen);
		m_pos += nLen;
		return string(pStr);
	};

	void readString(char* pDstStr)
	{
		const unsigned int nLen = readUint();
		memcpy(pDstStr, m_pBuffer + m_pos, nLen);
		m_pos += nLen;
	};

private:
	const char*	m_pBuffer;
	unsigned int	m_maxSize;
	unsigned int    m_pos;
};


static bool randomSrand = false;

class NZSocketUtil
{
public:
	static void writePackageHeader(NZBufferWriter* pBuffer)
	{
		char cFlag1 = random(1, BUFFER_FLAG_MAX) & BUFFER_MASK1;
		pBuffer->writeChar(cFlag1);
		char cFlag2 = random(1, BUFFER_FLAG_MAX) & BUFFER_MASK2;
		pBuffer->writeChar(cFlag2);
	};

	static void writePackageFooter(NZBufferWriter* pBuffer)
	{
		char cFlag1 = random(1, BUFFER_FLAG_MAX) & BUFFER_MASK3;
		pBuffer->writeChar(cFlag1);
		char cFlag2 = random(1, BUFFER_FLAG_MAX) & BUFFER_MASK4;
		pBuffer->writeChar(cFlag2);
	};

	static bool isPackageHeaderFlag1(const char* pChar)
	{
		char nValue = *pChar;
		char nValueMask = nValue & BUFFER_MASK1;
		return nValueMask == nValue;
	};

	static bool isPackageHeaderFlag2(const char* pChar)
	{
		char nValue = *pChar;
		char nValueMask = nValue & BUFFER_MASK2;
		return nValueMask == nValue;
	};

	static bool isPackageHeaderFlag3(const char* pChar)
	{
		char nValue = *pChar;
		char nValueMask = nValue & BUFFER_MASK3;
		return nValueMask == nValue;
	};

	static bool isPackageHeaderFlag4(const char* pChar)
	{
		char nValue = *pChar;
		char nValueMask = nValue & BUFFER_MASK4;
		return nValueMask == nValue;
	};

private:
	static const int random(const int &minNub, const int &maxNub)
	{
		int nMin = minNub;
		int nMax = maxNub;
		if (nMin > nMax)
		{
			swap(nMax, nMin);
		}
		if (!randomSrand)
		{
			srand(time(0));
			randomSrand = true;
		}
		return rand() % (nMax - nMin + 1) + nMin;
	};
};

#endif//__XC_NZSOCKETUTIL_H_
